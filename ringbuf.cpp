#include <iostream>

#include <vector>

#include "ringbuf.hpp"

int main()
{
//ds::ringbuf<std::vector<int>> r;
//r.container().resize(4);
//r.clear();
  ds::ringbuf<int[4], ds::rb::POW2> r;

  r.push(1);
  r.push(2);
  r.push(3);

  std::cout << "full: " << r.full() << " " << r.size() << std::endl;

  r.push(4);

  std::cout << "full: " << r.full() << " " << r.size() << std::endl;

  std::cout << r.front() << std::endl;
  r.pop();
  std::cout << r.front() << std::endl;
  r.pop();
  std::cout << r.front() << std::endl;
  r.pop();

  return 0;
}
