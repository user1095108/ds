#ifndef DS_RINGBUF_HPP
# define DS_RINGBUF_HPP
# pragma once

#include <cassert>

#include <iterator>

#include <type_traits>

namespace ds
{

namespace detail::ringbuf
{

template <typename T>
class defbase
{
protected:
  T c_;

  decltype(std::cbegin(c_)) r_{std::cbegin(c_)};
  decltype(std::begin(c_)) w_{std::begin(c_)};

  auto next(auto i) noexcept
  {
    i = std::next(i);

    return std::end(c_) == i ? std::begin(c_) : i;
  }

  auto next(auto i) const noexcept
  {
    i = std::next(i);

    return std::end(c_) == i ? std::begin(c_) : i;
  }

  //
  auto& get(decltype(r_) const r) const noexcept
  {
    return *r;
  }

  template <typename A>
  void set(decltype(w_) const w, A&& a) noexcept(
    noexcept(*w = std::forward<A>(a)))
  {
    *w = std::forward<A>(a);
  }
};

template <typename T>
class pow2base
{
protected:
  T c_;

  //assert(std::size(c_) && !(std::size(c_) & (std::size(c_) - 1)))
  std::size_t r_{};
  std::size_t w_{};

  auto next(decltype(r_) i) const noexcept
  {
    return (i + 1) & (std::size(c_) - 1);
  }

  //
  auto& get(decltype(r_) const r) const noexcept
  {
    return c_[r];
  }

  template <typename A>
  void set(decltype(w_) const w, A&& a) noexcept(
    noexcept(c_[w] = std::forward<A>(a)))
  {
    c_[w] = std::forward<A>(a);
  }
};

}

enum struct rb
{
  DEFAULT,
  POW2
};

template <typename T, enum rb rbt = rb::DEFAULT>
class ringbuf : std::conditional_t<rb::DEFAULT == rbt,
  detail::ringbuf::defbase<T>,
  detail::ringbuf::pow2base<T>
>
{
public:
  //
  auto& container() noexcept
  {
    return this->c_;
  }

  auto& container() const noexcept
  {
    return this->c_;
  }

  //
  void clear() noexcept
  {
    this->r_ = this->w_ = std::begin(this->c_);
  }

  bool empty() const noexcept
  {
    return this->r_ == this->w_;
  }

  bool full() const noexcept
  {
    return this->next(this->w_) == this->r_;
  }

  auto size() const noexcept
  {
    std::size_t s{};

    for (auto r(this->r_); r != this->w_; r = this->next(r), ++s);

    return s;
  }

  //
  auto& front() const noexcept
  {
    assert(!empty());
    return this->get(this->r_);
  }

  void pop() noexcept
  {
    assert(!empty());
    this->r_ = this->next(this->r_);
  }

  void push(auto&& a)
  {
    this->set(this->w_, std::forward<decltype(a)>(a));
    this->w_ = this->next(this->w_);

    if (empty()) // go to next recent
    {
      this->r_ = this->next(this->r_);
    }
  }
};

}

#endif // DS_RINGBUF_HPP
